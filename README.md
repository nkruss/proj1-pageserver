# README #

Author - Noah Kruss

Contact Info - nkruss@uoregon.edu

Software Description - A simple web server to send the contents of target html file stored in "pages" folder with proper http response. 
	If requested URL contains Forbidden characters such as  (~, .., //) server will transmits 403 forbidden error). 
	If requested file is not in target directory given by DOCROOT error code 404 not found will be transmitted.
	Otherwise contents of target file will be transmitted

